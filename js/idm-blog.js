
$(document).ready(function() {

    // Dims and resizes content, displays portable navigation
    $('#portable-hamburger').click(function() {
        $('#portable-nav').css('bottom', 'initial');
        var navHeight = $('#portable-nav').height();
        var viewportHeight = $(window).height();
        var renderHeight = 0;

        if (navHeight >= viewportHeight) {
            renderHeight = navHeight;
        } else {
            renderHeight = viewportHeight;
            $('#portable-nav').css('bottom', '0');
            $('#portable-nav div:last-of-type').addClass('absolute-bottom')
        }

        $('#page-wrapper').prepend('<button id="page-shadow"></button>');
        $('#page-wrapper').css('height', renderHeight);
        $('#portable-nav').css('transform', 'translateX(21rem)');
    });


    // Restores content size and brightness, hides portable navigation
    $(document).on('click', '#page-shadow', function() {
        $('#page-shadow').remove();
        $('html').removeClass('no-scroll');
        $('#page-wrapper').css('height', 'auto');
        $('#portable-nav').css('transform', 'translateX(0)');
        $('#portable-nav div:last-of-type').removeClass('absolute-bottom')
    });


    // Prints viewport dimensions to an span within a #viewport element on
    // document load
    // var viewportHeight = $(window).height();
    // var viewportWidth = $(window).width();
    // $('#viewport span').append('Height: ' + viewportHeight + ', Width: ' + viewportWidth);


    // Prints viewport dimensions to an span within a #viewport element on
    // screen resize
    // $(window).resize(function() {
    //     var viewportHeight = $(window).height();
    //     var viewportWidth = $(window).width();
    //     $('#viewport span').empty();
    //     $('#viewport span').append('Height: ' + viewportHeight + ', Width: ' + viewportWidth);
    // });

    //alert(navigator.userAgent);

});
